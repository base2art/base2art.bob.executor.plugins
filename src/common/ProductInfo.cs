// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyProduct("Base2art.Bob.Executor.Procedures")]
[assembly: ComVisible(false)]
