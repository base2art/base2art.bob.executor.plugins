﻿namespace Base2art.Bob.Executor.Plugin.ProcessExecution
{
    using System.ComponentModel;
    using Base2art.Bob.Executor.Procedures;
    
    public abstract class InvocationBase : Component
    {
        private readonly ProcedureLogger procedureLogger;

        protected InvocationBase(ProcedureLogger procedureLogger)
        {
            if (procedureLogger == null)
            {
                throw new System.ArgumentNullException("procedureLogger");
            }
            
            this.procedureLogger = procedureLogger;
        }

        public ProcedureLogger Logger
        {
            get { return this.procedureLogger; }
        }
        
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
