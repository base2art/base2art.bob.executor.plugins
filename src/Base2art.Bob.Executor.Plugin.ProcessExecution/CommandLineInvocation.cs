﻿namespace Base2art.Bob.Executor.Plugin.ProcessExecution
{
    using System.ComponentModel;
    using System.Globalization;
    using Base2art.Bob.Executor.Procedures;
    
    public class CommandLineInvocation : InvocationBase
    {
        private readonly ProcessExecutionStartInfo info;

        private readonly int passingReturnCode;

        public CommandLineInvocation(ProcessExecutionStartInfo info, ProcedureLogger procedureLogger, int passingReturnCode)
            : base(procedureLogger)
        {
            if (info == null)
            {
                throw new System.ArgumentNullException("info");
            }
            
            this.info = info;
            this.passingReturnCode = passingReturnCode;
        }

        public ProcessExecutionStartInfo ProcessExecutionStartInfo
        {
            get { return this.info; }
        }
        
        public void OnProcessExit(int exitCode)
        {
            if (exitCode != this.passingReturnCode)
            {
                string message = string.Format(
                    CultureInfo.InvariantCulture,
                    "Process Execution Failed: Expected ExitCode '{0}' Was '{1}'",
                    this.passingReturnCode,
                    exitCode);
                this.Logger.WriteDebug(message);
                throw new ProcessExecutionException(exitCode);
            }
        }
        
        public void OnInvocationStart()
        {
            var message = string.Format(
                CultureInfo.InvariantCulture,
                "Running console application: '{0}' in '{1}' with args '{2}'",
                this.info.Executable,
                this.info.WorkingDirectory,
                this.info.Arguments);

            this.Logger.WriteDebug(message);
        }

        public void OnInvocationEnd(int exitCode)
        {
            if (exitCode != this.passingReturnCode)
            {
                string message = string.Format(
                    CultureInfo.InvariantCulture,
                    "Process Execution Failed: Expected ExitCode '{0}' Was '{1}'",
                    this.passingReturnCode,
                    exitCode);
                this.Logger.WriteDebug(message);
                throw new ProcessExecutionException(exitCode);
            }
        }
    }
}
