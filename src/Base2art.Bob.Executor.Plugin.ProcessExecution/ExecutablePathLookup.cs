﻿namespace Base2art.Bob.Executor.Plugin.ProcessExecution
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Procedures.CustomTypes;
    
    public static class ExecutablePathLookup
    {
        public static DirectoryInfo[] GetDirectoryInfosForExecutables(
            string searchPath)
        {
            var items = GetFileInfosForExecutables(searchPath, false);
            
            HashSet<DirectoryInfo> dirs = new HashSet<DirectoryInfo>(new DirectoryInfoComparer());
            foreach (var file in items)
            {
                dirs.Add(file.Directory);
            }
            
            return dirs.ToArray();
        }
        
        public static FileInfo[] GetFileInfosForExecutables(
            string searchPath,
            bool includeMetadataFiles)
        {
            Func<string, string, bool> eq = (x, y) => string.Equals(x, y, StringComparison.OrdinalIgnoreCase);
            
            var searchPatterns =
                includeMetadataFiles
                ? new[] { "*.dll", "*.exe", "*.pdb", "*.config", "*.xml" }
                : new[] { "*.dll", "*.exe" };
            
            return GetFileInfosByWildcard(searchPath, searchPatterns)
                .Where(x =>
                       eq(x.Directory.Parent.Parent.Name, "bin") ||
                       eq(x.Directory.Parent.Name, "bin") ||
                       eq(x.Directory.Name, "bin"))
                .ToArray();
        }
        
        public static FileInfo[] GetFileInfosByWildcard(
            string searchPath,
            string[] searchPatterns)
        {
            var projectDir = new DirectoryInfo(searchPath);
            List<FileInfo> files = new List<FileInfo>();
            
            if (searchPatterns == null || searchPatterns.Length == 0)
            {
                return files.ToArray();
            }
            
            foreach (var searchPattern in searchPatterns)
            {
                files.AddRange(projectDir.GetFiles(searchPattern, SearchOption.AllDirectories));
            }
            
            return files.ToArray();
        }
        
        public static string WorkingDir(
            this ExecutableProcedureBaseParameters parameters,
            TargetExecutionParameters targetExecutionParameters)
        {
            return parameters.WorkingDirectory.HasValue
                ? parameters.WorkingDirectory.Value
                : targetExecutionParameters.DefaultWorkingDirectory;
        }

        public static string ExecutablePath(this ExecutableProcedureBaseParameters parameters)
        {
            return parameters.Executable.HasValue
                ? parameters.Executable.Value
                : null;
        }
        
        public static string PathInWorkingDir(
            this ExecutableProcedureBaseParameters parameters, 
            TargetExecutionParameters executionParameters, 
            PathString pathString, 
            string defaultPath, 
            bool throwExceptionOnNoPath)
        {
            var workingDir = parameters.WorkingDir(executionParameters);
            
            if (pathString.HasValue)
            {
                return Path.Combine(workingDir, pathString.Value);
            }
            
            if (throwExceptionOnNoPath)
            {
                throw new InvalidOperationException("Path is required and Found None.");
            }
            
            return Path.Combine(workingDir, defaultPath);
        }
        
        public static string PathInWorkingDir(
            this ExecutableProcedureBaseParameters parameters, TargetExecutionParameters executionParameters, PathString pathString, string defaultPath)
        {
            return parameters.PathInWorkingDir(executionParameters, pathString, defaultPath, false);
        }
        
        public static string PathInWorkingDir(
            this ExecutableProcedureBaseParameters parameters, TargetExecutionParameters executionParameters, PathString pathString)
        {
            return parameters.PathInWorkingDir(executionParameters, pathString, string.Empty, false);
        }
        
        public static string PathInWorkingDir(
            this ExecutableProcedureBaseParameters parameters, TargetExecutionParameters executionParameters, PathString pathString, bool throwExceptionOnNoPath)
        {
            return parameters.PathInWorkingDir(executionParameters, pathString, string.Empty, throwExceptionOnNoPath);
        }
        
        // Artifact
        public static string PathInArtifactDir(
            this TargetExecutionParameters executionParameters, 
            PathString pathString, 
            string defaultPath, 
            bool throwExceptionOnNoPath)
        {
            var artifactDir = executionParameters.DefaultArtifactDirectory;
            
            if (pathString.HasValue)
            {
                return Path.Combine(artifactDir, pathString.Value);
            }
            
            if (throwExceptionOnNoPath)
            {
                throw new InvalidOperationException("Path is required and Found None.");
            }
            
            return Path.Combine(artifactDir, defaultPath);
        }
        
        public static string PathInArtifactDir(
            this TargetExecutionParameters executionParameters, PathString pathString, string defaultPath)
        {
            return executionParameters.PathInArtifactDir(pathString, defaultPath, false);
        }
        
        public static string PathInArtifactDir(
            this TargetExecutionParameters executionParameters, PathString pathString)
        {
            return executionParameters.PathInArtifactDir(pathString, string.Empty, false);
        }
        
        public static string PathInArtifactDir(
            this TargetExecutionParameters executionParameters, PathString pathString, bool throwExceptionOnNoPath)
        {
            return executionParameters.PathInArtifactDir(pathString, string.Empty, throwExceptionOnNoPath);
        }
        
        // Not Archive
        public static string DerivePath<T>(
            TargetExecutionParameters targetExecutionParameters,
            T parameters,
            string executablePrefix,
            string[] x64Parts,
            string[] x86Parts = null,
            string packagesDir = "packages")
            where T : ExecutableProcedureBaseParameters
        {
            if (x86Parts == null)
            {
                x86Parts = new string[x64Parts.Length];
                Array.Copy(x64Parts, 0, x86Parts, 0, x64Parts.Length);
            }
            
            var packages = new DirectoryInfo(Path.Combine(targetExecutionParameters.DefaultWorkingDirectory, packagesDir));
            
            if (!packages.Exists)
            {
                return null;
            }
            
            var versions = new SortedDictionary<Version, string>();
            foreach (var packageItemDir in packages.EnumerateDirectories("*", SearchOption.TopDirectoryOnly))
            {
                if (packageItemDir.Name.StartsWith(executablePrefix, StringComparison.OrdinalIgnoreCase))
                {
                    var versionString = packageItemDir.Name.Substring(executablePrefix.Length);
                    Version version;
                    if (Version.TryParse(versionString, out version))
                    {
                        var consolePathx86 = packageItemDir.FullName;
                        var consolePathx64 = packageItemDir.FullName;
                        
                        consolePathx86 = x86Parts.Aggregate(consolePathx86, (x, y) => Path.Combine(x, y));
                        consolePathx64 = x64Parts.Aggregate(consolePathx64, (x, y) => Path.Combine(x, y));
                        
                        string consolePath = null;
                        
                        var tempConsolePath = parameters.Prefer64Bit
                            ? consolePathx64
                            : consolePathx86;
                        if (File.Exists(tempConsolePath))
                        {
                            consolePath = tempConsolePath;
                        }
                        
                        if (!string.IsNullOrWhiteSpace(consolePath))
                        {
                            versions[version] = consolePath;
                        }
                    }
                }
            }
            
            var keys = versions.Keys;
            if (keys.Count != 0)
            {
                var key = keys.Last();
                return versions[key];
            }
            
            return null;
        }
        
        private class DirectoryInfoComparer : IEqualityComparer<DirectoryInfo>
        {
            bool IEqualityComparer<DirectoryInfo>.Equals(DirectoryInfo x, DirectoryInfo y)
            {
                return string.Equals(x.FullName, y.FullName, StringComparison.OrdinalIgnoreCase);
            }
            
            int IEqualityComparer<DirectoryInfo>.GetHashCode(DirectoryInfo obj)
            {
                return obj.FullName.ToUpperInvariant().GetHashCode();
            }
        }
    }
}
