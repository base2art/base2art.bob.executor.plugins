﻿using System.Reflection;

[assembly: AssemblyTitle("Base2art.Bob.Executor.Plugin.ProcessExecution")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyDescription("An Api that runs executables as Procedures")]
[assembly: AssemblyConfiguration("")]