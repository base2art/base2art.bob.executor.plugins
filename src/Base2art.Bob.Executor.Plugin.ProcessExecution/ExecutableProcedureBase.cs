﻿namespace Base2art.Bob.Executor.Plugin.ProcessExecution
{
    using System.Collections.Generic;
    using System.IO;
    using Base2art.Bob.Executor.Procedures;

    public abstract class ExecutableProcedureBase<T> : Procedure<T>, IExecutableProcedure
        where T : ExecutableProcedureBaseParameters
    {
        string IExecutableProcedure.Arguments(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            object parameters,
            Dictionary<string, object> exportedData)
        {
            return this.Arguments(serviceLocator, targetExecutionParameters, (T)parameters, exportedData);
        }
        
        string IExecutableProcedure.WorkingDir(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            object parameters,
            Dictionary<string, object> exportedData)
        {
            return this.WorkingDir(serviceLocator, targetExecutionParameters, (T)parameters, exportedData);
        }
        
        string IExecutableProcedure.ExecutablePath(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            object parameters,
            Dictionary<string, object> exportedData)
        {
            return this.ExecutablePath(serviceLocator, targetExecutionParameters, (T)parameters, exportedData);
        }
        
        protected override IProcedureExecutionResult ExecuteProcedure(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            T parameters,
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
            var arguments = this.Arguments(serviceLocator, targetExecutionParameters, parameters, exportedData);
            var workingDir = this.WorkingDir(serviceLocator, targetExecutionParameters, parameters, exportedData);
            var relativeExePath = this.ExecutablePath(serviceLocator, targetExecutionParameters, parameters, exportedData);
            var fullExePath = Path.Combine(workingDir, relativeExePath);
            var info = new ProcessExecutionStartInfo(fullExePath)
            {
                Arguments = arguments,
                WorkingDirectory = workingDir
            };
            
            using (var commandLineInvocation = new CommandLineInvocation(info, procedureLogger, parameters.PassingReturnCode))
            {
                using (var host = new WindowsProcessHost(commandLineInvocation))
                {
                    host.Execute();
                }
            }
            
            return new ProcedureExecutionResult(true, this.ConvertVariables(exportedData));
        }
        
        protected abstract string Arguments(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            T parameters,
            Dictionary<string, object> exportedData);

        protected virtual string WorkingDir(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            T parameters,
            Dictionary<string, object> exportedData)
        {
            return parameters.WorkingDir(targetExecutionParameters);
        }

        protected virtual string ExecutablePath(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            T parameters,
            Dictionary<string, object> exportedData)
        {
            return parameters.ExecutablePath();
        }
    }
}
