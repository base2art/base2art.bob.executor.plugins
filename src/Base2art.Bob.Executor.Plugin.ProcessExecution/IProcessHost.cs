﻿namespace Base2art.Bob.Executor.Plugin.ProcessExecution
{
    using System.Threading.Tasks;
    
    public interface IProcessHost
    {
        void Execute();

        Task ExecuteAsync();
    }
}
