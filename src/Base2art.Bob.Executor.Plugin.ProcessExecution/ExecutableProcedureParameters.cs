﻿namespace Base2art.Bob.Executor.Plugin.ProcessExecution
{
    using Base2art.Bob.Executor.Procedures;
    
    [DependencyIdentifier("Base2art", "Executable-Parameters", "1.0.0.0")]
    public class ExecutableProcedureParameters : ExecutableProcedureBaseParameters
    {
        public string Arguments { get; set; }
    }
}
