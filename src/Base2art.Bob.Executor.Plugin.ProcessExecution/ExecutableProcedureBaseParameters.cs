﻿namespace Base2art.Bob.Executor.Plugin.ProcessExecution
{
    using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Procedures.CustomTypes;

    public class ExecutableProcedureBaseParameters : ProcedureParameters
    {
        public PathString WorkingDirectory { get; set; }

        public PathString Executable { get; set; }
        
        public bool Prefer64Bit { get; set; }

        public int PassingReturnCode { get; set; }
    }
}
