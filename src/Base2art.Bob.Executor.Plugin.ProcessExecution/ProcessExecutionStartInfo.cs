﻿namespace Base2art.Bob.Executor.Plugin.ProcessExecution
{
    public class ProcessExecutionStartInfo
    {
        private readonly string executable;

        public ProcessExecutionStartInfo(string executable)
        {
            this.executable = executable;
        }

        public string Executable
        {
            get { return this.executable; }
        }
        
        public string Arguments { get; set; }

        public string WorkingDirectory { get; set; }
    }
}
