﻿namespace Base2art.Bob.Executor.Plugin.ProcessExecution
{
    using System.Collections.Generic;
    using Base2art.Bob.Executor.Procedures;

    public interface IExecutableProcedure : IProcedure
    {
        string Arguments(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            object parameters,
            Dictionary<string, object> exportedData);

        string WorkingDir(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            object parameters,
            Dictionary<string, object> exportedData);

        string ExecutablePath(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            object parameters,
            Dictionary<string, object> exportedData);
    }
}
