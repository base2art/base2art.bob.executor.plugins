﻿namespace Base2art.Bob.Executor.Plugin.ProcessExecution
{
    using System;
    using System.ComponentModel;
    using System.Threading;
    using System.Threading.Tasks;
    
    public class WindowsProcessHost : Component, IProcessHost
    {
        private readonly ManualResetEvent done = new ManualResetEvent(false);
        
        private readonly CommandLineInvocation invocation;
        
        private volatile bool busy;
        
        public WindowsProcessHost(CommandLineInvocation invocation)
        {
            if (invocation == null)
            {
                throw new ArgumentNullException("invocation");
            }
            
            this.invocation = invocation;
        }
        
        public void Execute()
        {
            this.RunProcessAsync(
                this.invocation.ProcessExecutionStartInfo,
                this.invocation.Logger.Write,
                this.invocation.Logger.WriteError).Wait();
        }

        public async Task ExecuteAsync()
        {
            await this.RunProcessAsync(
                this.invocation.ProcessExecutionStartInfo,
                this.invocation.Logger.Write,
                this.invocation.Logger.WriteError);
        }
        
        public async Task RunProcessAsync(
            ProcessExecutionStartInfo startInfo,
            Action<string> handleDataWrite,
            Action<string> handleDataWriteError)
        {
            this.busy = true;
            this.invocation.OnInvocationStart();
            await Task.Factory.StartNew(delegate
                                        {
                                            int exitCode = 0;
                                            try
                                            {
                                                exitCode = CommandExecutor.ExecuteCommand(
                                                    startInfo.Executable,
                                                    startInfo.Arguments,
                                                    startInfo.WorkingDirectory,
                                                    handleDataWrite,
                                                    handleDataWriteError);
                                            }
                                            finally
                                            {
                                                this.busy = false;
                                                try
                                                {
                                                    this.invocation.OnInvocationEnd(exitCode);
                                                }
                                                finally
                                                {
                                                    if (!this.busy)
                                                    {
                                                        this.done.Set();
                                                    }
                                                }
                                            }
                                        });
        }
    }
}
