﻿namespace Base2art.Bob.Executor.Plugin.ProcessExecution
{
    using System.Collections.Generic;
    using Base2art.Bob.Executor.Procedures;
    
    [DependencyIdentifier("Base2art", "Executable", "1.0.0.0")]
    public class ExecutableProcedure : ExecutableProcedureBase<ExecutableProcedureParameters>
    {
        protected override string Arguments(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            ExecutableProcedureParameters parameters,
            Dictionary<string, object> exportedData)
        {
            return parameters.Arguments;
        }
    }
}
