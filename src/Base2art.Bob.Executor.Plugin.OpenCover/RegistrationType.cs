﻿namespace Base2art.Bob.Executor.Plugin.OpenCover
{
	public enum RegistrationType
	{
		User = 0,
		PassThrough = 1,
		Path32 = 2,
		Path64 = 3
	}
}