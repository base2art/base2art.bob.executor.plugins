﻿namespace Base2art.Bob.Executor.Plugin.OpenCover
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Plugin.ProcessExecution;

    [DependencyIdentifier("Base2art", "OpenCover", "1.0.0.0")]
    public class OpenCoverProcedure : ExecutableProcedureBase<OpenCoverProcedureParameters>
    {
        protected override string Arguments(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            OpenCoverProcedureParameters parameters,
            Dictionary<string, object> exportedData)
        {
            string executionOutputFile = null;
            
            if (parameters.OutputLocation.HasValue)
            {
                executionOutputFile = Path.Combine(
                    targetExecutionParameters.DefaultWorkingDirectory,
                    parameters.OutputLocation.Value);
            }
            else if (!string.IsNullOrWhiteSpace(targetExecutionParameters.DefaultArtifactDirectory))
            {
                var openCoverExecutionId = Guid.NewGuid().ToString("N");
                var original = Path.Combine(targetExecutionParameters.DefaultArtifactDirectory, openCoverExecutionId + ".codeCoverResults");
                int count = 0;
                while (File.Exists(original))
                {
                    count += 1;
                    original = Path.Combine(targetExecutionParameters.DefaultArtifactDirectory, openCoverExecutionId  + "_" + count + ".codeCoverResults");
                }
                
                executionOutputFile = original;
            }
            else
            {
                var openCoverExecutionId = Guid.NewGuid().ToString("N");
                
                var openCoverDir = Path.Combine(Path.GetTempPath(), "OpenCover");
                executionOutputFile = Path.Combine(openCoverDir, openCoverExecutionId +  ".codeCoverResults");
            }
            
            exportedData["CodeCoverage.OutputLocation"] = executionOutputFile;
            
            var testRunner = parameters.TestRunner;
            var proc = serviceLocator.ProcedureLocator.GetProcedure(testRunner.Procedure);
            var exeProc = proc as IExecutableProcedure;
            
            var innerArguments = exeProc.Arguments(serviceLocator, targetExecutionParameters, testRunner.Parameters, exportedData);
            var innerExe = exeProc.ExecutablePath(serviceLocator, targetExecutionParameters, testRunner.Parameters, exportedData);
            var innerDir = exeProc.WorkingDir(serviceLocator, targetExecutionParameters, testRunner.Parameters, exportedData);
            
            return string.Format(
                "{0} {1} {2} {3} {4} {5} {6} {7}",
                this.MapRegistrationType(parameters.RegistrationType),
                !parameters.NoMergeByHash ? "-mergebyhash" : string.Empty,
                string.Format("-targetdir:\"{0}\"", innerDir),
                string.Format("-target:\"{0}\"", innerExe),
                string.Format("-targetargs:\" {0} \"", innerArguments.Replace("\"", "\"\"").Trim()),
                string.Format("-filter:\"{0}\"", parameters.Filter),
                string.Format("-output:\"{0}\"", executionOutputFile),
                "-returntargetcode");
        }
        
        protected override string ExecutablePath(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            OpenCoverProcedureParameters parameters,
            Dictionary<string, object> exportedData)
        {
            var exe = parameters.Executable.HasValue
                ? parameters.Executable.Value
                : ExecutablePathLookup.DerivePath(
                    targetExecutionParameters,
                    parameters,
                    "OpenCover.",
                    new[] { "OpenCover.Console.exe" });
            
            if (string.IsNullOrWhiteSpace(exe) || !File.Exists(exe))
            {
                throw new ProcedureFailureException("OpenCover.Exe Does not exist '" + exe + "'");
            }
            
            return exe;
        }

        private string MapRegistrationType(RegistrationType registrationType)
        {
            switch (registrationType)
            {
                case RegistrationType.PassThrough:
                    return "-register";
                case RegistrationType.User:
                    return "-register:user";
                case RegistrationType.Path32:
                    return "-register:Path32";
                case RegistrationType.Path64:
                    return "-register:Path64";
            }
            
            throw new InvalidOperationException();
        }
    }
}

