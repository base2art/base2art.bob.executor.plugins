﻿namespace Base2art.Bob.Executor.Plugin.OpenCover
{
    using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Procedures.CustomTypes;
    using Base2art.Bob.Executor.Plugin.ProcessExecution;
    
    [DependencyIdentifier("Base2art", "OpenCover-Parameters", "1.0.0.0")]
    public class OpenCoverProcedureParameters : ExecutableProcedureBaseParameters
    {
        public RegistrationType RegistrationType { get; set; }

        public bool NoMergeByHash { get; set; }

        public ProcedureExecutionData TestRunner { get; set; }
        
        public string Filter { get; set; }
        
        public PathString OutputLocation { get; set; }
    }
}