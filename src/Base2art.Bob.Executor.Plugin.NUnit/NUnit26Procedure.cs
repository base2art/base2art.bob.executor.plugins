﻿namespace Base2art.Bob.Executor.Plugin.NUnit
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Plugin.ProcessExecution;
    
    [DependencyIdentifier("Base2art", "NUnit", "2.6.0.0")]
    public class NUnit26Procedure : ExecutableProcedureBase<NUnit26ProcedureParameters>
    {
        protected override DefaultLifecyclePhase SuggestedDefaultLifecyclePhase
        {
            get {  return DefaultLifecyclePhase.Test; }
        }
        
        protected override string Arguments(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            NUnit26ProcedureParameters parameters,
            Dictionary<string, object> exportedData)
        {
            var outputDir = Path.Combine(Path.GetTempPath(), "NUnit");
            var outputFile = Path.Combine(outputDir, Guid.NewGuid().ToString());
            
            Directory.CreateDirectory(outputDir);
            
            var arguments = string.Format(
                "{0} /result={1} /nologo /process={2:G} /domain={3:G} {4} {5}",
                GetTestAssemblies(serviceLocator, targetExecutionParameters, parameters, exportedData),
                outputFile,
                parameters.ProcessIsolation,
                parameters.AppDomainCreationStrategy,
                parameters.AllowShadowCopy ? string.Empty : "/noshadow",
                parameters.Arguments);
            
            return arguments;
        }
        
        protected override string ExecutablePath(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            NUnit26ProcedureParameters parameters,
            Dictionary<string, object> exportedData)
        {
            var exe = parameters.Executable.HasValue
                ? parameters.Executable.Value
                : ExecutablePathLookup.DerivePath(
                    targetExecutionParameters,
                    parameters,
                    "NUnit.Runners.",
                    new[] { "tools", "nunit-console.exe" },
                    new[] { "tools", "nunit-console-x86.exe" });
            
            if (string.IsNullOrWhiteSpace(exe) || !File.Exists(exe))
            {
                throw new ProcedureFailureException("Nunit-Console Exe Does not exist '" + exe + "'");
            }
            
            return exe;
        }

        private string GetTestAssemblies(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            NUnit26ProcedureParameters parameters,
            Dictionary<string, object> exportedData)
        {
            IEnumerable<FileInfo> files = ExecutablePathLookup.GetFileInfosForExecutables(targetExecutionParameters.DefaultWorkingDirectory, includeMetadataFiles: false);
            
            if (!parameters.NoDefaultTestAssemblyFilters)
            {
                files = files.Where(x => !string.Equals(x.Name, "nunit.framework.dll"));
            }
            
            if (!string.IsNullOrWhiteSpace(parameters.StartsWithFilter))
            {
                files = files.Where(x => x.Name.StartsWith(parameters.StartsWithFilter, StringComparison.InvariantCultureIgnoreCase));
            }
            
            if (!string.IsNullOrWhiteSpace(parameters.EndsWithFilter))
            {
                files = files.Where(x => x.Name.EndsWith(parameters.EndsWithFilter, StringComparison.InvariantCultureIgnoreCase));
            }
            
            return string.Join(" ", files.Select(x => "\"" + x.FullName + "\""));
        }
        
        private class CustomKeyedCollection : KeyedCollection<string, FileInfo>
        {
            public CustomKeyedCollection()
                :base(StringComparer.OrdinalIgnoreCase)
            {
                
            }
            
            public string GetKeyForItem1(FileInfo item)
            {
                return item.Name.ToUpperInvariant();
            }
            
            protected override string GetKeyForItem(FileInfo item)
            {
                return item.Name.ToUpperInvariant();
            }
        }
    }
}