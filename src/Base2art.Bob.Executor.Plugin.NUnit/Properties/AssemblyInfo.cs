﻿using System.Reflection;

[assembly: AssemblyTitle("Base2art.Bob.Executor.Plugin.NUnit")]
[assembly: AssemblyVersion("2.6.0.0")]
[assembly: AssemblyFileVersion("2.6.0.0")]
[assembly: AssemblyDescription("Executes NUnit")]
[assembly: AssemblyConfiguration("")]