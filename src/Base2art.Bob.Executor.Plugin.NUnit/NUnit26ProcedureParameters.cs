﻿namespace Base2art.Bob.Executor.Plugin.NUnit
{
	using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Plugin.ProcessExecution;

	[DependencyIdentifier("Base2art", "NUnit-Parameters", "2.6.0.0")]
	public class NUnit26ProcedureParameters : ExecutableProcedureParameters
	{
        public ProcessIsolation ProcessIsolation { get; set; }

        public AppDomainCreationStrategy AppDomainCreationStrategy { get; set; }

        public bool AllowShadowCopy { get; set; }

        public bool NoDefaultTestAssemblyFilters { get; set; }

        public string StartsWithFilter { get; set; }

        public string EndsWithFilter { get; set; }
	}
}

