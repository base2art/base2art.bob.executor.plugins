﻿using System.Reflection;

[assembly: AssemblyTitle("Base2art.Bob.Executor.Plugin.ReportGenerator")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyDescription("Generates Code Coverage Reports")]
[assembly: AssemblyConfiguration("")]