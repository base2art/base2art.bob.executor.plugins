﻿namespace Base2art.Bob.Executor.Plugin.ReportGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Plugin.ProcessExecution;

    [DependencyIdentifier("Base2art", "ReportGenerator", "1.0.0.0")]
    public class ReportGeneratorProcedure : ExecutableProcedureBase<ReportGeneratorProcedureParameters>
    {
        protected override IProcedureExecutionResult ExecuteProcedure(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            ReportGeneratorProcedureParameters parameters,
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
            var result = parameters.CoverageRunner.ExecuteProcedure(
                serviceLocator,
                targetExecutionParameters,
                procedureLogger);
            
            if (result.IsSuccessfulRun.HasValue && !result.IsSuccessfulRun.Value)
            {
                return result;
            }
            
            var parentResult = base.ExecuteProcedure(serviceLocator, targetExecutionParameters, parameters, procedureLogger, exportedData);
            
            Console.WriteLine("NunitSuccess");
            Console.WriteLine(parentResult.IsSuccessfulRun);
            return new ProcedureExecutionResult(
                parentResult.IsSuccessfulRun, 
                this.ConvertVariables(exportedData, result.ExportedData));
        }
        
        protected override string Arguments(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            ReportGeneratorProcedureParameters parameters,
            Dictionary<string, object> exportedData)
        {
            string sourceDir = targetExecutionParameters.DefaultWorkingDirectory;
            var di = new DirectoryInfo(sourceDir);
            var dis = new List<DirectoryInfo>();
            this.GetAll(dis, di);
            sourceDir = string.Join(";", dis.Select(x=>x.FullName));
            
            var subProcedureData = exportedData
                .Where(x => x.Key.StartsWith("SubProcedure::", StringComparison.InvariantCultureIgnoreCase))
                .Where(x => !x.Key.StartsWith("SubProcedure::SubProcedure::", StringComparison.InvariantCultureIgnoreCase));
            
            var location = Location(targetExecutionParameters, subProcedureData);
            
            string targetDir = "c:\\Temp\\coverage";
            
            if (!string.IsNullOrWhiteSpace(targetExecutionParameters.DefaultArtifactDirectory))
            {
                targetDir = Path.Combine(
                    targetExecutionParameters.DefaultArtifactDirectory,
                    "codeCoverage");
            }
            
            return string.Format(
                "{0} {1} {2} {3}",
                string.Format("\"-targetdir:{0}\"", targetDir),
                string.Format("\"-sourcedirs:{0}\"", sourceDir),
                string.Format("-reporttypes:{0}", "Html"),
                string.Format("-reports:{0}", location));
        }
        
        protected override string ExecutablePath(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            ReportGeneratorProcedureParameters parameters,
            Dictionary<string, object> exportedData)
        {
            var exe = parameters.Executable.HasValue
                ? parameters.Executable.Value
                : ExecutablePathLookup.DerivePath(
                    targetExecutionParameters,
                    parameters,
                    "ReportGenerator.",
                    new[] { "ReportGenerator.exe" });
            
            if (string.IsNullOrWhiteSpace(exe) || !File.Exists(exe))
            {
                throw new ProcedureFailureException("ReportGenerator.exe Does not exist '" + exe + "'");
            }
            
            return exe;
        }

        private void GetAll(List<DirectoryInfo> dis, DirectoryInfo di)
        {
            dis.Add(di);
            foreach (var cdi in di.GetDirectories())
            {
                this.GetAll(dis, cdi);
            }
        }

        private static string Location(TargetExecutionParameters parameters, IEnumerable<KeyValuePair<string, object>> subProcedureData)
        {
            if (!string.IsNullOrWhiteSpace(parameters.DefaultArtifactDirectory))
            {
                var dir = new DirectoryInfo(parameters.DefaultArtifactDirectory);
                var files = dir.GetFiles("*.codeCoverResults");
                if (files.Length > 0)
                {
                    return files[0].FullName;
                }
            }
            
            
            
            Func<IEnumerable<KeyValuePair<string, object>>, string> lookup = pairs =>
            {
                if (pairs.Count() == 1)
                {
                    var pair = pairs.First();
                    var pv = pair.Value as string;
                    return string.IsNullOrWhiteSpace(pv) ? null : pv;
                }
                
                return null;
            };
            
            Func<string, string, bool> contains = (source, searchText) => CultureInfo.InvariantCulture.CompareInfo.IndexOf(source, searchText, CompareOptions.IgnoreCase) >= 0;
            
            var value = lookup(subProcedureData);
            if (!string.IsNullOrWhiteSpace(value))
            {
                return value;
            }
            
            value = lookup(subProcedureData.Where(x => x.Key.Contains("CodeCoverage.OutputLocation")));
            if (!string.IsNullOrWhiteSpace(value))
            {
                return value;
            }
            
            
            
            throw new InvalidOperationException("Could Not Find the OutputLocation");
        }
    }
    
}

