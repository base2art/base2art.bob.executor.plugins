﻿namespace Base2art.Bob.Executor.Plugin.ReportGenerator
{
    using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Procedures.CustomTypes;
    using Base2art.Bob.Executor.Plugin.ProcessExecution;
    
    [DependencyIdentifier("Base2art", "ReportGenerator-Parameters", "1.0.0.0")]
    public class ReportGeneratorProcedureParameters : ExecutableProcedureBaseParameters
    {
        public ProcedureExecutionData CoverageRunner { get; set; }
    }
}