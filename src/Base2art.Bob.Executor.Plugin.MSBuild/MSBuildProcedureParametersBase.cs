﻿namespace Base2art.Bob.Executor.Plugin.MSBuild
{
    using System;
    using System.Linq;
    using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Procedures.CustomTypes;

    public abstract class MSBuildProcedureParametersBase : ProcedureParameters
    {
        public virtual PathString WorkingDirectory { get; set; }

        public virtual PathString BuildFile { get; set; }

        public virtual int PassingReturnCode { get; set; }

        public virtual string[] Targets { get; set; }

        public virtual PathString Executable { get; set; }
        
        public virtual string[] BuildArgs { get; set; }
        
        public virtual string Configuration { get; set; }
    }
}
