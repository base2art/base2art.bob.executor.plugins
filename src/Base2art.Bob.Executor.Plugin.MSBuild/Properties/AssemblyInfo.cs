﻿using System.Reflection;

[assembly: AssemblyTitle("Base2art.Bob.Executor.Plugin.MSBuild")]
[assembly: AssemblyVersion("4.0.0.0")]
[assembly: AssemblyFileVersion("4.0.0.0")]
[assembly: AssemblyDescription("Builds using MSBuild")]
[assembly: AssemblyConfiguration("")]