﻿namespace Base2art.Bob.Executor.Plugin.MSBuild
{
    using System;
    using System.Linq;
    using Base2art.Bob.Executor.Procedures;

    [DependencyIdentifier("Base2art", "MSBuild-Clean-Parameters", "4.0.0.0")]
    public class MSBuildCleanProcedureParameters : MSBuildProcedureParametersBase
    {
        public MSBuildCleanProcedureParameters()
        {
            this.Targets = new[] { "Clean" };
        }

        public override string[] BuildArgs
        {
            get
            {
                return base.BuildArgs ?? new[]
                {
                    "/verbosity:quiet",
                    "/m",
                    "/nologo"
                };
            }
            
            set
            {
                base.BuildArgs = value;
            }
        }

        public override string Configuration
        {
            get
            {
                return string.IsNullOrWhiteSpace(base.Configuration) ? "Release" : base.Configuration;
            }
            
            set
            {
                base.Configuration = value;
            }
        }
    }
}
