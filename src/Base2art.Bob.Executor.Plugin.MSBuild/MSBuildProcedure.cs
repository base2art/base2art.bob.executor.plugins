﻿namespace Base2art.Bob.Executor.Plugin.MSBuild
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using Base2art.Bob.Executor.Plugin.ProcessExecution;
    using Base2art.Bob.Executor.Procedures;
    
    [DependencyIdentifier("Base2art", "MSBuild", "4.0.0.0")]
    public class MSBuildProcedure : Procedure<MSBuildProcedureParametersBase>
    {
        protected override CleanLifecyclePhase SuggestedCleanLifecyclePhase
        {
            get { return CleanLifecyclePhase.Clean; }
        }
        
        protected override DefaultLifecyclePhase SuggestedDefaultLifecyclePhase
        {
            get { return DefaultLifecyclePhase.Compile; }
        }
        
        public static string DeriveMSBuildPath()
        {
            var windowsDir = Environment.GetFolderPath(Environment.SpecialFolder.Windows);
            var netDir = Path.Combine(windowsDir, "Microsoft.NET");
            
            var frameworkDir = Path.Combine(netDir, "Framework64");
            
            if (!Directory.Exists(frameworkDir))
            {
                frameworkDir = Path.Combine(netDir, "Framework");
            }
            
            var frameworkDirInfo = new DirectoryInfo(frameworkDir);
            var versions = new SortedDictionary<Version, string>();
            
            foreach (var dir in frameworkDirInfo.EnumerateDirectories())
            {
                var msBuildPath = Path.Combine(dir.FullName, "MSBuild.exe");
                if (!File.Exists(msBuildPath))
                {
                    continue;
                }
                
                if (dir.Name.StartsWith("v", StringComparison.Ordinal))
                {
                    Version version;
                    if (Version.TryParse(dir.Name.Substring(1), out version))
                    {
                        versions[version] = msBuildPath;
                    }
                }
            }
            
            return versions[versions.Keys.Last()];
        }
        
        protected override IProcedureExecutionResult ExecuteProcedure(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            MSBuildProcedureParametersBase parameters,
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
            var arguments = string.Format(
                CultureInfo.InvariantCulture,
                "{0} {1} /t:{2} /p:Configuration={3}",
                parameters.BuildFile.Value,
                string.Join(" ", parameters.BuildArgs ?? new string[0]),
                string.Join(";", parameters.Targets ?? new[] { "Rebuild" }),
                parameters.Configuration);
            
            var workingDir = parameters.WorkingDirectory.HasValue
                ? parameters.WorkingDirectory.Value
                : targetExecutionParameters.DefaultWorkingDirectory;
            
            var exe = parameters.Executable.HasValue
                ? parameters.Executable.Value
                : DeriveMSBuildPath();
            
            var info = new ProcessExecutionStartInfo(exe)
            {
                Arguments = arguments,
                WorkingDirectory = workingDir
            };
            
            using (var commandLineInvocation = new CommandLineInvocation(info, procedureLogger, parameters.PassingReturnCode))
            {
                using (var host = new WindowsProcessHost(commandLineInvocation))
                {
                    host.Execute();
                    return new ProcedureExecutionResult(true, this.ConvertVariables(exportedData));
                }
            }
        }
    }
}
