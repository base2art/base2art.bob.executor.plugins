﻿namespace Base2art.Bob.Executor.Plugin.MSBuild
{
    using System;
    using System.Linq;
    using Base2art.Bob.Executor.Procedures;
    
    [DependencyIdentifier("Base2art", "MSBuild-Parameters", "4.0.0.0")]
    public class MSBuildProcedureParameters : MSBuildProcedureParametersBase
    {
        public MSBuildProcedureParameters()
        {
            this.Targets = new[] { "Rebuild" };
        }

        public override string[] BuildArgs
        {
            get
            {
                return base.BuildArgs ?? new[]
                {
                    "/verbosity:quiet",
                    "/m",
                    "/nologo"
                };
            }
            
            set
            {
                base.BuildArgs = value;
            }
        }

        public override string Configuration
        {
            get
            {
                return string.IsNullOrWhiteSpace(base.Configuration) ? "Release" : base.Configuration;
            }
            
            set
            {
                base.Configuration = value;
            }
        }
    }
}
