﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Xml.Linq;
    using Base2art.Bob.Executor.Procedures;

    [DependencyIdentifier("Base2art", "NuGet-Package-Pack", "1.0.0.0")]
    public class NuGetPackagePackProcedure : NuGetPackageProcedureBase<NuGetPackagePackProcedureParameters>
    {
        protected override DefaultLifecyclePhase SuggestedDefaultLifecyclePhase
        {
            get
            {
                return DefaultLifecyclePhase.GenerateResources;
            }
        }

        protected override string Arguments(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            NuGetPackagePackProcedureParameters parameters,
            Dictionary<string, object> exportedData)
        {
            var relOutputDir = 
                !string.IsNullOrWhiteSpace(targetExecutionParameters.DefaultArtifactDirectory)
                ? targetExecutionParameters.DefaultArtifactDirectory
                : parameters.OutputDirectory.GetValueOrDefault("artifacts");
            
            var relInput = parameters.NuspecOrProjectFile.GetValueOrDefault(string.Empty);
            
            var specFile = Path.Combine(targetExecutionParameters.DefaultWorkingDirectory, relInput);
            var outputDir = Path.Combine(targetExecutionParameters.DefaultWorkingDirectory, relOutputDir);
            
            var outputDirectory = string.Format("-OutputDirectory \"{0}\"", outputDir.TrimEnd('\\'));
            
            Directory.CreateDirectory(outputDir);
            
            if (parameters.VersionPullLocation == VersionPullLocation.NuspecOrProjectFile)
            {
                return string.Format("pack \"{0}\" {1}", specFile, outputDirectory);
            }
            
            var spec = this.ReadSpecFile(specFile);
            
            
            var files = from file in spec.Root.Descendants("file")
                where file.Attribute("src").Value.EndsWith(".exe", StringComparison.InvariantCultureIgnoreCase) || 
                      file.Attribute("src").Value.EndsWith(".dll", StringComparison.InvariantCultureIgnoreCase)
                select 
                      file.Attribute("src").Value;
            
            
            var fileValue = files.FirstOrDefault();
            
            if (fileValue == null)
            {
                return string.Format("pack \"{0}\" {1}", specFile, outputDirectory);
            }
            
            var dllPath = Path.Combine(Path.GetDirectoryName(specFile), fileValue);
            if (!File.Exists(dllPath))
            {
                return string.Format("pack \"{0}\" {1}", specFile, outputDirectory);
            }
            
            var version = this.GetVersion(dllPath, parameters.VersionPullLocation);
            return string.Format(
                "pack \"{0}\" {1} -Version {2}",
                specFile,
                outputDirectory,
                version);
        }

        private XDocument ReadSpecFile(string specFile)
        {
            using (var inStream = new FileStream(specFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                return XDocument.Load(inStream);
            }
        }

        private string GetVersion(string dllPath, VersionPullLocation versionPullLocation)
        {
            var asm = Assembly.ReflectionOnlyLoadFrom(dllPath);
            
            if (versionPullLocation == VersionPullLocation.AssemblyVersion)
            {
                return asm.GetCustomAttributes(typeof(AssemblyVersionAttribute), false)
                    .OfType<AssemblyVersionAttribute>()
                    .Select(x => x.Version)
                    .FirstOrDefault();
            }
            
            if (versionPullLocation == VersionPullLocation.AssemblyFileVersion)
            {
                return asm.GetCustomAttributes(typeof(AssemblyFileVersionAttribute), false)
                    .OfType<AssemblyFileVersionAttribute>()
                    .Select(x => x.Version)
                    .FirstOrDefault();
            }
            
            if (versionPullLocation == VersionPullLocation.AssemblyInformationalVersion)
            {
                return asm.GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), false)
                    .OfType<AssemblyInformationalVersionAttribute>()
                    .Select(x => x.InformationalVersion)
                    .FirstOrDefault();
            }
            
            return string.Empty;
        }
    }
}

/*
		    
			var formatString = new StringBuilder("install \"{0}\"");
			var outputDirectory = parameters.PackagesDirectory.HasValue ? parameters.PackagesDirectory.Value : "packages";
			formatString.Append(" -OutputDirectory \"{1}\"");
			if (!string.IsNullOrWhiteSpace(parameters.PackageVersion))
			{
				formatString.Append(" -Version {2}");
			}
			return string.Format(formatString.ToString(), parameters.PackageName, outputDirectory, parameters.PackageVersion);
 */
