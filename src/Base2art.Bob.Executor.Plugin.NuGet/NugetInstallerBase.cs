﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Base2art.Bob.Executor.Procedures;

    public abstract class NuGetInstallerBase
    {
        private readonly NuGetVersion version;

        protected NuGetInstallerBase(NuGetVersion version)
        {
            this.version = version;
        }

        protected virtual string ExeStreamName
        {
            get
            {
                return "Base2art.Bob.Executor.Plugin.NuGet.Versions." + this.version + ".NuGet.exe";
            }
        }

        public virtual void WriteNuGetExe(IProcedureLogger taskLogger, string nugetPath)
        {
            var message = string.Format("Writing nuget.exe ({1}) to: '{0}'", nugetPath, this.version);
            taskLogger.WriteDebug(message);
            var asm = Assembly.GetExecutingAssembly();
            using (var stream = asm.GetManifestResourceStream(this.ExeStreamName))
            {
                using (var fh = File.OpenWrite(nugetPath))
                {
                    stream.CopyTo(fh);
                    fh.Flush();
                }
            }
        }
    }
}



