﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
    using System;
    using Base2art.Bob.Executor.Procedures;
    
    [DependencyIdentifier("Base2art", "NuGet-Package-Install-Parameters", "1.0.0.0")]
    public class NuGetPackageInstallProcedureParameters : NuGetPackageProcedureParameters
    {
        /// <summary>
        /// optional but recommended
        /// </summary>
        public string PackageVersion { get; set; }

        /// <summary>
        /// Required
        /// </summary>
        public string PackageName { get; set; }
    }
}
