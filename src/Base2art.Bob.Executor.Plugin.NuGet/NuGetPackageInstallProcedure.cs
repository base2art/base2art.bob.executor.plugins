﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Base2art.Bob.Executor.Procedures;
    
    [DependencyIdentifier("Base2art", "NuGet-Package-Install", "1.0.0.0")]
    public class NuGetPackageInstallProcedure : NuGetPackageProcedureBase<NuGetPackageInstallProcedureParameters>
    {
        protected override DefaultLifecyclePhase SuggestedDefaultLifecyclePhase
        {
            get { return DefaultLifecyclePhase.Package; }
        }

        protected override string Arguments(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters, 
            NuGetPackageInstallProcedureParameters parameters,
            Dictionary<string, object> exportedData)
        {
            var formatString = new StringBuilder("install \"{0}\"");
            
            var outputDirectory = parameters.PackagesDirectory.HasValue
                ? parameters.PackagesDirectory.Value
                : "packages";
            
            formatString.Append(" -OutputDirectory \"{1}\"");
            
            
            if (!string.IsNullOrWhiteSpace(parameters.PackageVersion))
            {
                formatString.Append(" -Version {2}");
            }
            
            return string.Format(
                formatString.ToString(), 
                parameters.PackageName, 
                outputDirectory, 
                parameters.PackageVersion);
        }
    }
    
}