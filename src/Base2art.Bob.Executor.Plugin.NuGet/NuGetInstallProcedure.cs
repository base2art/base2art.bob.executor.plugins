﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
    using System;
    using System.IO;
    using Base2art.Bob.Executor.Procedures;

    [DependencyIdentifier("Base2art", "NuGet-Install", "1.0.0.0")]
    public class NuGetInstallProcedure : ProcedureAction<NuGetInstallProcedureParameters>
    {
        protected override void ExecuteProcedureAction(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            NuGetInstallProcedureParameters parameters,
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
            var workingDirectory = targetExecutionParameters.DefaultWorkingDirectory;
            
            var outputLocation = Path.Combine(".nuget", "nuget.exe");
            
            if (parameters.OutputLocation.HasValue)
            {
                outputLocation = parameters.OutputLocation.Value;
            }
            
            var nugetDir = Path.Combine(workingDirectory, outputLocation);
            Directory.CreateDirectory(Path.GetDirectoryName(nugetDir));
            
            var installer = this.CreateInstaller(parameters);
            installer.WriteNuGetExe(procedureLogger, nugetDir);
        }

        protected virtual NuGetInstallerBase CreateInstaller(NuGetInstallProcedureParameters taskData)
        {
            var version = taskData.NuGetVersion;
            if (version == NuGetVersion.v2_8_6)
            {
                return new NuGet.Versions.v2_8_6.Installer();
            }
            
            if (version == NuGetVersion.v2_8_2)
            {
                return new NuGet.Versions.v2_8_2.Installer();
            }
            
            throw new InvalidOperationException("Cannot Create Installer for version '" + version.ToString("G") +"'");
        }
    }
}

