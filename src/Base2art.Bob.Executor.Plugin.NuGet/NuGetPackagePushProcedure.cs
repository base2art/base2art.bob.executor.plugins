﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
	using System;
	using System.Collections.Generic;
    using System.IO;
	using System.Text;
	using Base2art.Bob.Executor.Procedures;

	[DependencyIdentifier("Base2art", "NuGet-Package-Push", "1.0.0.0")]
	public class NuGetPackagePushProcedure : NuGetPackageProcedureBase<NuGetPackagePushProcedureParameters>
	{
		protected override DefaultLifecyclePhase SuggestedDefaultLifecyclePhase
		{
			get { return DefaultLifecyclePhase.Install; }
		}

		protected override string Arguments(
		    ProcedureServiceLocator serviceLocator, 
		    TargetExecutionParameters targetExecutionParameters, 
		    NuGetPackagePushProcedureParameters parameters, 
		    Dictionary<string, object> exportedData)
		{
		    var formatString = new StringBuilder();
			string packageLocation = parameters.PackageLocation.HasValue
			    ? Path.Combine(targetExecutionParameters.DefaultWorkingDirectory,  parameters.PackageLocation.Value)
			    : Path.Combine(targetExecutionParameters.DefaultWorkingDirectory,  "artifacts", "*.nupkg");
			
			var timespan = parameters.Timeout.HasValue
			    ? parameters.Timeout.Value
			    : TimeSpan.FromMinutes(5);
			
			return string.Format(
			    "push \"{0}\" {1} {2} {3} {4} {5}",
			    packageLocation, 
			    parameters.ApiKey,
			    "-NonInteractive",
			    string.Format("-Source \"{0}\"", parameters.Source),
			    string.Format("-Timeout \"{0}\"", timespan.TotalSeconds),
			    string.Format("-ConfigFile \"{0}\"", parameters.ConfigFile));
		}
	}
}

