﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
	public enum VersionPullLocation
	{
		NuspecOrProjectFile,
		AssemblyInformationalVersion,
		AssemblyVersion,
		AssemblyFileVersion
	}
}





