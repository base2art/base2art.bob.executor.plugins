﻿namespace Base2art.Bob.Executor.Plugin.NuGet.Versions.v2_8_2
{
    public class Installer : NuGetInstallerBase
    {
        public Installer() : base(NuGetVersion.v2_8_2)
        {
            
        }
    }
}
