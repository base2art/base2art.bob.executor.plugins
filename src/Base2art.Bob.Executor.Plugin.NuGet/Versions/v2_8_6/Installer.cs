﻿namespace Base2art.Bob.Executor.Plugin.NuGet.Versions.v2_8_6
{
    public class Installer : NuGetInstallerBase
    {
        public Installer() : base(NuGetVersion.v2_8_6)
        {
            
        }
    }
}
