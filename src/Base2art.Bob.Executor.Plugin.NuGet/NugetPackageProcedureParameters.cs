﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
	using System;
	using Base2art.Bob.Executor.Procedures.CustomTypes;
    using Base2art.Bob.Executor.Plugin.ProcessExecution;

	public class NuGetPackageProcedureParameters : ExecutableProcedureBaseParameters
	{
		/// <summary>
		/// Optional
		/// </summary>
		public PathString PackagesDirectory { get; set; }
	}
}



