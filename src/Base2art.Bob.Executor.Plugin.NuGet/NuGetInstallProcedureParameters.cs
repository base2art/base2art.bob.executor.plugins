﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
	using System;
	using Base2art.Bob.Executor.Procedures;
	using Base2art.Bob.Executor.Procedures.CustomTypes;

	[DependencyIdentifier("Base2art", "NuGet-Install-Parameters", "1.0.0.0")]
	public class NuGetInstallProcedureParameters : ProcedureParameters
	{
		public PathString OutputLocation { get; set; }

        public NuGetVersion NuGetVersion { get; set; }
	}
	
}



