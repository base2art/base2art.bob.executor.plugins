﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Xml.Linq;
    using Base2art.Bob.Executor.Procedures;

    [DependencyIdentifier("Base2art", "NuGet-Configure", "1.0.0.0")]
    public class NuGetConfigureProcedure : ProcedureAction<NuGetConfigureProcedureParameters>
    {

        protected virtual string ConfigStreamName
        {
            get { return "Base2art.Bob.Plugin.NuGet.Versions.NuGet.config"; }
        }
        
        protected override void ExecuteProcedureAction(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            NuGetConfigureProcedureParameters parameters,
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
            var workingDirectory = targetExecutionParameters.DefaultWorkingDirectory;
            
            
            var localNugetDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Nuget");
            Directory.CreateDirectory(localNugetDir);
            
            var localNugetConfigPath = Path.Combine(localNugetDir, "NuGet.config");
            
            this.WriteNuGetConfig(procedureLogger, localNugetConfigPath, parameters.SourceName, parameters.SourceLocation);
        }

        public virtual void WriteNuGetConfig(IProcedureLogger taskLogger, string nugetConfigPath, string sourceName, string sourceLocation)
        {
            if (!File.Exists(nugetConfigPath))
            {
                var message = string.Format("Installing nuget.config to: '{0}'", nugetConfigPath);
                taskLogger.WriteDebug(message);
                this.InstallNuGetConfig(nugetConfigPath);
            }
            
            {
                var message = string.Format("Updating nuget.config to: '{0}'", nugetConfigPath);
                taskLogger.WriteDebug(message);
                this.UpdateNuGetConfig(nugetConfigPath, sourceName, sourceLocation);
            }
        }
        
        

        protected virtual void UpdateNuGetConfig(string nugetConfigPath, string sourceName, string sourceLocation)
        {
            var nugetDoc = XDocument.Load(nugetConfigPath);
            
            Func<string, bool> install = x =>
            {
                var el = nugetDoc.Root.Element(x);
                if (el == null)
                {
                    el = new XElement(x);
                    nugetDoc.Root.Add(el);
                }
                
                var items = el.Elements(Xml.Add);
                if (!items.Any(y => y.Attribute(Xml.Key) != null && string.Equals(y.Attribute(Xml.Key).Value, sourceName, StringComparison.InvariantCultureIgnoreCase)))
                {
                    el.Add(new XElement(Xml.Add, new XAttribute(Xml.Key, sourceName), new XAttribute(Xml.Value, sourceLocation)));
                    return true;
                }
                
                return false;
            };
            
            var a = install(Xml.ActivePackageSource);
            var b = install(Xml.PackageSources);
            
            if (a | b)
            {
                nugetDoc.Save(nugetConfigPath);
            }
        }

        protected virtual void InstallNuGetConfig(string nugetConfigPath)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            using (var stream = asm.GetManifestResourceStream(this.ConfigStreamName))
            {
                var doc = XDocument.Load(stream);
                doc.Save(nugetConfigPath);
            }
        }
        
        private static class Xml
        {
//            public const string LocalPackageCache = "LocalPackageCache";

            public const string PackageSources = "packageSources";

            public const string ActivePackageSource = "activePackageSource";

            public const string Add = "add";

            public const string Key = "key";

            public const string Value = "value";
        }
    }
}



