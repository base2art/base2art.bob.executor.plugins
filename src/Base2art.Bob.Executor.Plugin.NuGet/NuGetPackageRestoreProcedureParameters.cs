﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
    using System;
    using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Procedures.CustomTypes;

    [DependencyIdentifier("Base2art", "NuGet-Package-Restore-Parameters", "1.0.0.0")]
    public class NuGetPackageRestoreProcedureParameters : NuGetPackageProcedureParameters
    {
        public PathString SolutionOrPackagesFile { get; set; }

        public string Options { get; set; }
    }
}

