﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
	using System;
	using Base2art.Bob.Executor.Procedures;

	[DependencyIdentifier("Base2art", "NuGet-Configure-Parameters", "1.0.0.0")]
	public class NuGetConfigureProcedureParameters : ProcedureParameters
	{
        public string SourceName { get; set; }
        
        public string SourceLocation { get; set; }
	}
}





