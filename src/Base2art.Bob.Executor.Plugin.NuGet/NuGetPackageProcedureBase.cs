﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Plugin.ProcessExecution;

    public abstract class NuGetPackageProcedureBase<T> : ExecutableProcedureBase<T>
        where T : NuGetPackageProcedureParameters
    {
//	{
//		protected override void ExecuteProcedureAction(TargetExecutionParameters targetExecutionParameters, T parameters, ProcedureLogger procedureLogger)
//		{
//			var arguments = this.Arguments(targetExecutionParameters, parameters);
//			var workingDir = this.WorkingDir(targetExecutionParameters, parameters);
//			var relativeExePath = this.RelativeNugetPath(parameters);
//			var fullExePath = Path.Combine(workingDir, relativeExePath);
//			var info = new ProcessExecutionStartInfo(fullExePath)
//			{
//				Arguments = arguments,
//				WorkingDirectory = workingDir
//			};
//
//			using (var commandLineInvocation = new CommandLineInvocation(info, procedureLogger, parameters.PassingReturnCode))
//			{
//				using (var host = new WindowsProcessHost(commandLineInvocation))
//				{
//					host.Execute();
//				}
//			}
//		}

//		protected abstract string Arguments(TargetExecutionParameters targetExecutionParameters, T parameters);
//
//		protected virtual string WorkingDir(TargetExecutionParameters targetExecutionParameters, NuGetPackageProcedureParameters parameters)
//		{
//			var workingDir = targetExecutionParameters.DefaultWorkingDirectory;
//			if (parameters.WorkingDirectory.HasValue)
//			{
//				workingDir = parameters.WorkingDirectory.Value;
//			}
//			return workingDir;
//		}
//
        protected override string ExecutablePath(
            ProcedureServiceLocator serviceLocator, 
            TargetExecutionParameters targetExecutionParameters,
            T parameters,
            Dictionary<string, object> exportedData)
        {
            var relativeExePath = Path.Combine(".nuget", "nuget.exe");
            if (parameters.Executable.HasValue)
            {
                relativeExePath = parameters.Executable.Value;
            }
            return relativeExePath;
        }
    }
}

