﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
	using System;
	using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Procedures.CustomTypes;

	[DependencyIdentifier("Base2art", "NuGet-Package-Pack-Parameters", "1.0.0.0")]
	public class NuGetPackagePackProcedureParameters : NuGetPackageProcedureParameters
	{
		public VersionPullLocation VersionPullLocation { get; set; }

		public PathString NuspecOrProjectFile { get; set; }
		
		public PathString OutputDirectory { get; set; }
	}
}



