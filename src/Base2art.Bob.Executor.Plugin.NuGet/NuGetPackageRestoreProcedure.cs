﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
	using System;
    using System.Collections.Generic;
	using Base2art.Bob.Executor.Procedures;

	[DependencyIdentifier("Base2art", "NuGet-Package-Restore", "1.0.0.0")]
	public class NuGetPackageRestoreProcedure : NuGetPackageProcedureBase<NuGetPackageRestoreProcedureParameters>
	{
		protected override DefaultLifecyclePhase SuggestedDefaultLifecyclePhase
		{
		    get { return DefaultLifecyclePhase.Initialize; }
		}

		protected override string Arguments(
		    ProcedureServiceLocator serviceLocator, 
		    TargetExecutionParameters targetExecutionParameters, 
		    NuGetPackageRestoreProcedureParameters parameters,
            Dictionary<string, object> exportedData)
		{
			if (parameters.SolutionOrPackagesFile.HasValue)
			{
				return string.Format("restore \"{0}\" {1}", parameters.SolutionOrPackagesFile, parameters.Options);
			}
			
			return string.Format("restore {0}", parameters.Options);
		}
	}
}



