﻿namespace Base2art.Bob.Executor.Plugin.NuGet
{
    using System;
    using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Procedures.CustomTypes;

    [DependencyIdentifier("Base2art", "NuGet-Package-Push-Parameters", "1.0.0.0")]
    public class NuGetPackagePushProcedureParameters : NuGetPackageProcedureParameters
    {
        /// <summary>
        /// optional but recommended
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Required
        /// </summary>
        public string ApiKey { get; set; }

        /// <summary>
        /// Required
        /// </summary>
        public TimeSpan? Timeout { get; set; }

        /// <summary>
        /// optional
        /// </summary>
        public string ConfigFile { get; set; }

        /// <summary>
        /// optional but recommended
        /// </summary>
        public PathString PackageLocation { get; set; }
    }
}



