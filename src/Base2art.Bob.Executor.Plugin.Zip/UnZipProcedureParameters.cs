﻿namespace Base2art.Bob.Executor.Plugin.Zip
{
    using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Procedures.CustomTypes;

    [DependencyIdentifier("Base2art", "Un-Zip-Parameters", "1.0.0.0")]
    public class UnZipProcedureParameters : ProcedureParameters
    {
        /// <summary>
        /// Default is '/'
        /// </summary>
        public PathString OutputPath { get; set; }
        
        /// <summary>
        /// Required
        /// </summary>
        public PathString InputFile { get; set; }
        
        /// <summary>
        /// Required
        /// </summary>
        public BaseDirectory InputBaseDirectory { get; set; }
        
        /// <summary>
        /// Required
        /// </summary>
        public BaseDirectory OutputBaseDirectory { get; set; }
    }
}
