﻿namespace Base2art.Bob.Executor.Plugin.Zip
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using Base2art.Bob.Executor.Procedures;
    
    [DependencyIdentifier("Base2art", "Zip-UnZip", "1.0.0.0")]
    public class UnZipProcedure : ProcedureAction<UnZipProcedureParameters>
    {
        protected override DefaultLifecyclePhase SuggestedDefaultLifecyclePhase
        {
            get {  return DefaultLifecyclePhase.None; }
        }
        
        protected override void ExecuteProcedureAction(
            ProcedureServiceLocator serviceLocator, 
            TargetExecutionParameters targetExecutionParameters, 
            UnZipProcedureParameters parameters, 
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
            if (!parameters.InputFile.HasValue)
            {
                throw new ProcedureFailureException("Zipping... Input Path Required");
            }
            
            if (!parameters.OutputPath.HasValue)
            {
                throw new ProcedureFailureException("Zipping... Output File Required");
            }
            
            var @inputBase = parameters.InputBaseDirectory == BaseDirectory.ArtifactDirectory
                ? targetExecutionParameters.DefaultArtifactDirectory
                : targetExecutionParameters.DefaultWorkingDirectory;
            
            var @outputBase = parameters.OutputBaseDirectory == BaseDirectory.ArtifactDirectory
                ? targetExecutionParameters.DefaultArtifactDirectory
                : targetExecutionParameters.DefaultWorkingDirectory;
            
            var input = Path.Combine(@inputBase, parameters.InputFile.Value);
            var output = Path.Combine(@outputBase, parameters.OutputPath.Value);
            
            ZipFile.ExtractToDirectory(input, output);
        }
    }
}