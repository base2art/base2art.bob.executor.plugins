﻿namespace Base2art.Bob.Executor.Plugin.Zip
{
    public enum BaseDirectory
    {
        WorkingDirectory = 0,
        ArtifactDirectory = 1,
    }
}
