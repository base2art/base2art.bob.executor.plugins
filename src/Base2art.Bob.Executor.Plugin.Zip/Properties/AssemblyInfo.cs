﻿using System.Reflection;

[assembly: AssemblyTitle("Base2art.Bob.Executor.Plugin.Zip")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyDescription("A set up procedures that zip and unzip")]
[assembly: AssemblyConfiguration("")]