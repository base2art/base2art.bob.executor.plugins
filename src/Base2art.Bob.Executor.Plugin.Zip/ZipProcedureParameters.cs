﻿namespace Base2art.Bob.Executor.Plugin.Zip
{
	using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Procedures.CustomTypes;

	[DependencyIdentifier("Base2art", "Zip-Parameters", "1.0.0.0")]
	public class ZipProcedureParameters : ProcedureParameters
	{
        /// <summary>
        /// Default is '/'
        /// </summary>
        public PathString OutputFile { get; set; }
        
        /// <summary>
        /// Required
        /// </summary>
        public PathString InputPath { get; set; }
        
        /// <summary>
        /// Required
        /// </summary>
        public BaseDirectory InputBaseDirectory { get; set; }
        
        /// <summary>
        /// Required
        /// </summary>
        public BaseDirectory OutputBaseDirectory { get; set; }
	}
	
	
}

