﻿namespace Base2art.Bob.Executor.Plugin.Zip
{
    using System;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using Base2art.Bob.Executor.Procedures;
    
    [DependencyIdentifier("Base2art", "Zip", "1.0.0.0")]
    public class ZipProcedure : ProcedureAction<ZipProcedureParameters>
    {
        protected override DefaultLifecyclePhase SuggestedDefaultLifecyclePhase
        {
            get {  return DefaultLifecyclePhase.None; }
        }
        
        protected override void ExecuteProcedureAction(
            ProcedureServiceLocator serviceLocator, 
            TargetExecutionParameters targetExecutionParameters, 
            ZipProcedureParameters parameters, 
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
            if (!parameters.InputPath.HasValue)
            {
                throw new ProcedureFailureException("Zipping... Input Path Required");
            }
            if (!parameters.OutputFile.HasValue)
            {
                throw new ProcedureFailureException("Zipping... Output File Required");
            }
            
            var @inputBase = parameters.InputBaseDirectory == BaseDirectory.ArtifactDirectory
                ? targetExecutionParameters.DefaultArtifactDirectory
                : targetExecutionParameters.DefaultWorkingDirectory;
            
            var @outputBase = parameters.OutputBaseDirectory == BaseDirectory.ArtifactDirectory
                ? targetExecutionParameters.DefaultArtifactDirectory
                : targetExecutionParameters.DefaultWorkingDirectory;
            
            var input = Path.Combine(@inputBase, parameters.InputPath.Value);
            var output = Path.Combine(@outputBase, parameters.OutputFile.Value);
            
            ZipFile.CreateFromDirectory(input, output, CompressionLevel.Optimal, false);
//            var compiledWebDir = targetExecutionParameters.PathInArtifactDir(parameters.TargetDirectory, "compiled-web");
        }
    }
}