﻿namespace Base2art.Bob.Executor.Plugin.AspNetCompiler
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base2art.Bob.Executor.Plugin.ProcessExecution;
    using Base2art.Bob.Executor.Procedures;
    
    [DependencyIdentifier("Base2art", "AspNetCompiler", "1.0.0.0")]
    public class AspNetCompilerProcedure : ExecutableProcedureBase<AspNetCompilerProcedureParameters>
    {
        protected override DefaultLifecyclePhase SuggestedDefaultLifecyclePhase
        {
            get {  return DefaultLifecyclePhase.PreparePackage; }
        }
        
        protected override string ExecutablePath(ProcedureServiceLocator serviceLocator, TargetExecutionParameters targetExecutionParameters, AspNetCompilerProcedureParameters parameters, Dictionary<string, object> exportedData)
        {
            var path = base.ExecutablePath(serviceLocator, targetExecutionParameters, parameters, exportedData);
            return string.IsNullOrWhiteSpace(path) ? Utilities.DeriveCompilerPath() : path;
        }
        
        protected override string WorkingDir(ProcedureServiceLocator serviceLocator, TargetExecutionParameters targetExecutionParameters, AspNetCompilerProcedureParameters parameters, Dictionary<string, object> exportedData)
        {
            return parameters.PathInWorkingDir(targetExecutionParameters, parameters.PhysicalPath, true);
        }
        
        protected override string Arguments(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            AspNetCompilerProcedureParameters parameters,
            Dictionary<string, object> exportedData)
        {
            var keySwitch = parameters.SigningKeyPath.HasValue
                ? string.Format("-keyfile \"{0}\"", parameters.PathInWorkingDir(targetExecutionParameters, parameters.SigningKeyPath))
                : "";
            
            var compiledWebDir = targetExecutionParameters.PathInArtifactDir(parameters.TargetDirectory, "compiled-web");
            var arguments = string.Format(
                "-errorstack -nologo {0} {1} -v \"{2}\" {3} {4} {5} {6} \"{7}\"",
                parameters.ForceRebuild ? "-c" : "",
                keySwitch,
                string.IsNullOrWhiteSpace(parameters.VirtualPath) ? "/" : parameters.VirtualPath,
                parameters.Arguments,
                parameters.ForceDebug ? "-d" : "",
                parameters.Updatable ? "-u" : "",
                parameters.OverridePreviousBuilds ? "-f" : "",
                compiledWebDir);
            
            return arguments;
        }
    }
}