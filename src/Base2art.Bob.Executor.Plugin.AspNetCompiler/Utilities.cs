﻿namespace Base2art.Bob.Executor.Plugin.AspNetCompiler
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    public static class Utilities
    {
        public static string DeriveCompilerPath()
        {
            var windowsDir = Environment.GetFolderPath(Environment.SpecialFolder.Windows);
            var netDir = Path.Combine(windowsDir, "Microsoft.NET");
            
            var frameworkDir = Path.Combine(netDir, "Framework64");
            
            if (!Directory.Exists(frameworkDir))
            {
                frameworkDir = Path.Combine(netDir, "Framework");
            }
            
            var frameworkDirInfo = new DirectoryInfo(frameworkDir);
            var versions = new SortedDictionary<Version, string>();
            
            foreach (var dir in frameworkDirInfo.EnumerateDirectories())
            {
                var msBuildPath = Path.Combine(dir.FullName, "aspnet_compiler.exe");
                if (!File.Exists(msBuildPath))
                {
                    continue;
                }
                
                if (dir.Name.StartsWith("v", StringComparison.Ordinal))
                {
                    Version version;
                    if (Version.TryParse(dir.Name.Substring(1), out version))
                    {
                        versions[version] = msBuildPath;
                    }
                }
            }
            
            return versions[versions.Keys.Last()];
        }
    }
}
