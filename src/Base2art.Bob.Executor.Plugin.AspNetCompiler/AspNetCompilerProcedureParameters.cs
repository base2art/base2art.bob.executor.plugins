﻿namespace Base2art.Bob.Executor.Plugin.AspNetCompiler
{
    using Base2art.Bob.Executor.Plugin.ProcessExecution;
	using Base2art.Bob.Executor.Procedures;
    using Base2art.Bob.Executor.Procedures.CustomTypes;

	[DependencyIdentifier("Base2art", "AspNetCompiler-Parameters", "1.0.0.0")]
	public class AspNetCompilerProcedureParameters : ExecutableProcedureParameters
	{
        public bool Updatable { get; set; }
        
        public bool OverridePreviousBuilds { get; set; }
        
        public bool ForceDebug { get; set; }
        
        public bool ForceRebuild { get; set; }

        public PathString SigningKeyPath { get; set; }

        /// <summary>
        /// Default is '/'
        /// </summary>
        public string VirtualPath { get; set; }

        /// <summary>
        /// Default is '/'
        /// </summary>
        public PathString TargetDirectory { get; set; }
        
        /// <summary>
        /// Required
        /// </summary>
        public PathString PhysicalPath { get; set; }
	}
}

