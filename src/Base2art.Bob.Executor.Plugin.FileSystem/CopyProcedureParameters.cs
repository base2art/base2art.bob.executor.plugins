﻿namespace Base2art.Bob.Executor.Plugin.FileSystem
{
	using Base2art.Bob.Executor.Procedures;
	using Base2art.Bob.Executor.Procedures.CustomTypes;

	[DependencyIdentifier("Base2art", "FileSystem-Copy-Parameters", "1.0.0.3")]
	public class CopyProcedureParameters : ProcedureParameters
	{
		/// <summary>
		/// Default is '/'
		/// </summary>
		public PathString OutputPath { get; set; }

		/// <summary>
		/// Required
		/// </summary>
		public PathString InputPath { get; set; }

		/// <summary>
		/// Required
		/// </summary>
		public BaseDirectory InputBaseDirectory { get; set; }

		/// <summary>
		/// Required
		/// </summary>
		public BaseDirectory OutputBaseDirectory { get; set; }
		
		/// <summary>
		/// Default is false
		/// </summary>
		public bool OutputPathIsDirectory { get; set; }
	}
}



