﻿namespace Base2art.Bob.Executor.Plugin.FileSystem
{
    using System;
    using System.IO;
    using System.Linq;
    using Base2art.Bob.Executor.Procedures;
    
    [DependencyIdentifier("Base2art", "FileSystem-Copy", "1.0.0.3")]
    public class CopyProcedure : ProcedureAction<CopyProcedureParameters>
    {
        protected override DefaultLifecyclePhase SuggestedDefaultLifecyclePhase
        {
            get {  return DefaultLifecyclePhase.None; }
        }
        
        protected override void ExecuteProcedureAction(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            CopyProcedureParameters parameters,
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
            if (!parameters.InputPath.HasValue)
            {
                throw new ProcedureFailureException("Copy Failed... Input Path Required");
            }
            
            if (!parameters.OutputPath.HasValue)
            {
                throw new ProcedureFailureException("Copy Failed... Output Path Required");
            }
            
            var @inputBase = parameters.InputBaseDirectory == BaseDirectory.ArtifactDirectory
                ? targetExecutionParameters.DefaultArtifactDirectory
                : targetExecutionParameters.DefaultWorkingDirectory;
            
            var @outputBase = parameters.OutputBaseDirectory == BaseDirectory.ArtifactDirectory
                ? targetExecutionParameters.DefaultArtifactDirectory
                : targetExecutionParameters.DefaultWorkingDirectory;
            
            var input = Path.Combine(@inputBase, parameters.InputPath.Value);
            var output = Path.Combine(@outputBase, parameters.OutputPath.Value);
            
            if (File.Exists(input))
            {
                if (parameters.OutputPathIsDirectory)
                {
                    Directory.CreateDirectory(output);
                    output = Path.Combine(output, Path.GetFileName(input));
                }

                File.Copy(input, output);
            }
            else if (Directory.Exists(input))
            {
                DirectoryCopy(input, output, true);
            }
            else
            {
                throw new DirectoryNotFoundException(
                    "Source file/directory does not exist or could not be found: "
                    + input);
            }
        }
        
        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();


            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
    }
}